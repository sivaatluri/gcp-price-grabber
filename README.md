#AWS Lambda Price Grabber

## Prerequisites for developers computer

Ubuntu 18.04.1 LTS Operating system. 

The python in this repository runs under Python 2.7

To setup source control:

sudo apt-get install git 
git config --global user.name "Neil Spink" 
git config --global user.email "neilspink@gmail.com" 
git clone git@gitlab.com:neilspink/aws-lambda-price-grabber.git 

## About

This project is part of a multi week project and about running an Google functions and AWS lambda function that 
read the price off an online shop.

This repository is a fork off https://gitlab.com/neilspink/aws-lambda-price-grabber