
# Commands for developing and testing the AWS cloud formation.

Do not use your AWS root account ever! You need to go into the AWS IAM console and create an admin user, if you don't 
already have one. You need a pair of access keys which you can generate from the security credentials tab when looking
at the user profile.

## Install the AWS CLI and configure.

pip install awscli
aws configure

## Setup AWS for GitLab

You can upload the aws-create-lambda-json in the AWS CloudFormation console or use these commands to create S3 bucket, 
the user for the GitLab pipeline and required permissions to deploy the solution.

If you use these commands, then you will need to change the 'ParameterValue' which is the S3 Bucket Name. You need to 
update the name in the manual deployment section below and in the files; aws-create-lambda.json and .gitlab.ci.yml too

aws cloudformation create-stack --stack-name grabber-foundation  --template-body file://./deployment/aws-create-foundation.json --capabilities CAPABILITY_NAMED_IAM --parameters ParameterKey=S3BucketName,ParameterValue=aws-lambda-price-grabber

Once the stack is created you need to go into the IAM console -> Security Credentials tab and "create access key".
Sign into GitLab, then settings -> CI / CD -> Variables and add AWS_ACCESS_KEY_ID & AWS_SECRET_ACCESS_KEY making them
protected and giving in the access key you just created.


The grab_invoke function has been hardcoded to the S3 Bucket and needs updating. Also the role arn information needs to
be updated.

You must update the .gitlab.ci.yml file with the S3 bucket name, after which you can in GitLab run the pipeline under
CI / CD or using the commands in the following section.

## Manual Solution Deployment

These allow you deploy as GitLab does and to develop it further. It faster to use the CLI than wait on GitLab to do the job

aws s3 cp ./test.zip s3://aws-lambda-price-grabber/

aws configure set default.region eu-central-1

aws configure set aws_access_key_id YOUR-SECRET-ID

aws configure set aws_secret_access_key YOUR-SECRET-KEY

aws cloudformation package --template-file ./deployment/aws-create-lambda.json --s3-bucket aws-lambda-price-grabber --output-template template-export.yml

aws cloudformation deploy --template-file template-export.yml --stack-name aws-lambda-price-grabber-stack --capabilities CAPABILITY_IAM

aws cloudformation delete-stack --stack-name aws-lambda-price-grabber-stack
